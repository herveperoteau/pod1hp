//
//  Pod1HPTests.m
//  Pod1HPTests
//
//  Created by Herve Peroteau on 24/11/2018.
//  Copyright © 2018 Herve Peroteau. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <Pod1HP/Pod1HP.h>

@interface Pod1HPTests : XCTestCase

@end

@implementation Pod1HPTests

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    [HPConsole printLog:@"coucou"];
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
