//
//  HPConsole.m
//  Pod1HP
//
//  Created by Herve Peroteau on 24/11/2018.
//  Copyright © 2018 Herve Peroteau. All rights reserved.
//

#import "HPConsole.h"
#import "DateTools.h"

@implementation HPConsole

+ (void)printLog:(NSString *)log {
    // Use dependency with DateTools
    NSString *dateStr = [[NSDate date] formattedDateWithStyle:NSDateFormatterFullStyle];
    // Print in console
    NSLog(@"%@ --> %@", dateStr, log);
}

@end
