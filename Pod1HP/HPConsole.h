//
//  HPConsole.h
//  Pod1HP
//
//  Created by Herve Peroteau on 24/11/2018.
//  Copyright © 2018 Herve Peroteau. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HPConsole : NSObject

+ (void)printLog:(NSString *)log;

@end

NS_ASSUME_NONNULL_END
