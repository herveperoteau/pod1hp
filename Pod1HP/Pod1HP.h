//
//  Pod1HP.h
//  Pod1HP
//
//  Created by Herve Peroteau on 24/11/2018.
//  Copyright © 2018 Herve Peroteau. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for Pod1HP.
FOUNDATION_EXPORT double Pod1HPVersionNumber;

//! Project version string for Pod1HP.
FOUNDATION_EXPORT const unsigned char Pod1HPVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Pod1HP/PublicHeader.h>
#import <Pod1HP/HPConsole.h>


